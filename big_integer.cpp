#include "big_integer.h"
#include <cstddef>
#include <cstring>
#include <ostream>
#include <stdexcept>
#include <string>
#include <limits>
#include <functional>


big_integer::big_integer() = default;

void big_integer::signed_constructor(int64_t b)
{
  if (b == std::numeric_limits<int64_t>::min()) {
    constructor(static_cast<uint64_t>(b));
  }
  else {
    constructor(static_cast<uint64_t>(b < 0 ? -b : b));
  }
}

void big_integer::constructor(uint64_t b) {
  while (b != 0) {
    data.push_back(b);
    b >>= BASE_SIZE;
  }
  trim();
}

big_integer::big_integer(big_integer const& other) = default;

big_integer::big_integer(int a)
    : sign(a < 0)
{
  signed_constructor(a);
}

big_integer::big_integer(unsigned long long a)
    : sign(false)
{
  constructor(a);
}

big_integer::big_integer(unsigned int a)
    : sign(false)
{
  constructor(a);
}
big_integer::big_integer(long long a)
    : sign(a < 0)
{
  signed_constructor(a);
}

big_integer::big_integer(long a)
    : sign(a < 0)
{
  signed_constructor(a);
}

big_integer::big_integer(unsigned long a)
    : sign(false)
{
  constructor(a);
}


big_integer::big_integer(std::string const& str) {
  data.push_back(0);
  size_t start = 0;
  if (str.length() == 0) {
    throw std::invalid_argument("Empty string");
  }
  if (str[0] == '-' || str[0] == '+') {
    if (str.length() == 1) {
      throw std::invalid_argument("Invalid argument for Big Integer");
    }
    else {
      start = 1;
    }
  }
  std::vector<uint32_t> res;
  for (size_t i = start; i < str.length(); i++) {
    if (std::isdigit(str[i])) {
      res.push_back(int(str[i]) - '0');
    }
    else {
      throw std::invalid_argument("Invalid argument for Big Integer");
    }
  }

  for (size_t i = 0; i < res.size(); i++) {
    mul_int(10);
    add_int(res[i]);
  }
  sign = str[0] == '-' ? 1 : 0;
  trim();
}

big_integer::~big_integer() = default;

void big_integer::trim() {
  while (!data.empty() && !data.back()) {
    data.pop_back();
  }
  if (data.empty()) {
    sign = false;
  }
}

bool big_integer::is_zero() {
  return data.empty();
}

void big_integer::swap(big_integer& b) {
  std::swap(data, b.data);
  std::swap(sign, b.sign);
}


big_integer& big_integer::operator=(big_integer const& other) {
  if (this == &other) {
    return *this;
  }
  data = other.data;
  sign = other.sign;
  return *this;
}

big_integer& big_integer::abs_add(big_integer const& rhs) {
  uint64_t carry = 0;
  size_t max_length = std::max(rhs.length(), length());
  for (size_t i = 0; i < max_length; i++) {
    if (i == length()) {
      data.push_back(0);
    }
    else if ((i >= rhs.length() || i >= length()) && carry == 0) {
      break;
    }
    carry += (i < rhs.length() ? rhs.data[i] : 0) + static_cast<uint64_t>(data[i]);
    data[i] = static_cast<uint32_t>(carry);
    carry >>= BASE_SIZE;
  }
  if (carry != 0) {
    data.push_back(static_cast<uint32_t>(carry));
  }
  return *this;
}

big_integer& big_integer::abs_subtract(big_integer const& rhs) {
  uint32_t borrow = 0, b_temp = 0;
  big_integer const *p1 = this;
  big_integer const *p2 = &rhs;

  if (rhs.abs_greater(*this)) {
    std::swap(p1, p2);
    sign = !sign;
  }

  for (size_t i = 0; i < std::max(length(), rhs.length()); i++) {
    if (i == length()) {
      data.push_back(rhs.data[i]);
    }
    if (i >= rhs.length()) {
      data[i] -= borrow;
      break;
    }
    b_temp = (p2->data[i] == MAX_DIGIT && borrow != 0) ||
             p2->data[i] + borrow > p1->data[i];
    data[i] = p1->data[i] - p2->data[i] - borrow;
    borrow = b_temp;
  }
  trim();
  return *this;
}

big_integer& big_integer::operator+=(big_integer const& rhs) {  //wrong
  if (sign == rhs.sign) {
    return abs_add(rhs);
  }
  return abs_subtract(rhs);
}

big_integer& big_integer::operator-=(big_integer const& rhs) {
  if (sign == rhs.sign) {
    return abs_subtract(rhs);
  }
  return abs_add(rhs);
}

big_integer& big_integer::operator*=(big_integer const& rhs) {
  uint64_t mul = 0;
  big_integer res;
  res.data.resize(rhs.length() + length(), 0);
  for (size_t i = 0; i < rhs.length(); i++) {
    for (size_t j = 0; j < length() + 1; j++) {
      mul += (j == length() ? 0 :
          static_cast<uint64_t>(data[j]) * rhs.data[i] + res.data[i + j]);
      res.data[i + j] = static_cast<uint32_t>(mul);
      mul >>= BASE_SIZE;
    }
  }
  res.sign = sign != rhs.sign;
  swap(res);
  trim();
  return *this;
}

void big_integer::add_int(uint32_t a) {
  if (is_zero()) {
    data.push_back(a);
    return;
  }
  uint64_t cur = a;
  for (size_t i = 0; i < length(); i++) {
    cur += static_cast<uint64_t>(data[i]);
    data[i] = static_cast<uint32_t>(cur);
    cur >>= BASE_SIZE;
    if (cur == 0) {
      break;
    }
  }
  if (cur != 0) {
    data.push_back(cur);
  }
  trim();
}

void big_integer::mul_int(uint32_t a) {
  uint64_t cur = 0;
  for (size_t i = 0; i < length(); i++) {
    cur += static_cast<uint64_t>(data[i]) * a;
    data[i] = static_cast<uint32_t>(cur);
    cur >>= BASE_SIZE;
  }
  if (cur != 0) {
    data.push_back(cur);
  }
  trim();
}

void big_integer::div_int(uint32_t a) {
  uint64_t r = 0;
  for (size_t i = length(); i >= 1; --i) {
    uint64_t cur = data[i - 1] + (r << BASE_SIZE);
    data[i - 1] = cur / a;
    r = static_cast<uint32_t>(cur % a);
  }
  trim();
}

std::pair<big_integer, big_integer> big_integer::divmod(big_integer const& rhs) {
  if (length() < rhs.length()) {
    return {0, *this};
  }
  if (is_zero()) {
    return {0, 0};
  }
  uint32_t norm =  BASE / (static_cast<uint64_t>(rhs.data.back()) + 1);
  big_integer a = *this * norm;
  big_integer b = rhs * norm;
  a.sign = false;
  b.sign = false;
  big_integer q, r;
  q.data.resize(a.length(), 0);

  for (int64_t j = a.length() - 1; j >= 0; j--) {
    r <<= BASE_SIZE;
    r += a.data[j];
    uint32_t q1 = r.length() <= b.length()? 0 : r.data[b.length()];
    uint32_t q2 = r.length() <= b.length() - 1 ? 0 : r.data[b.length() - 1];
    uint32_t d = ((static_cast<uint64_t>(q1) << BASE_SIZE) + q2) / b.data.back();
    r -= b * d;

    while (r < 0) {
      r += b;
      d--;
    }
    q.data[j] = d;
  }
  q.sign = sign != rhs.sign;
  r.sign = sign;
  r.div_int(norm);
  q.trim();
  r.trim();
  return {q, r};
}

big_integer& big_integer::operator/=(big_integer const& rhs) {
  *this = divmod(rhs).first;
  return *this;
}

big_integer& big_integer::operator%=(big_integer const& rhs) {
  *this = divmod(rhs).second;
  return *this;
}

void big_integer::twos_complement(big_integer & res) const {
  if (!sign) {
    res = *this;
    return;
  }
  res.sign = sign;
  res.data.resize(length(), 0);
  for (size_t i = 0; i < length(); i++) {
    res.data[i] = data[i] ^ MAX_DIGIT;
  }
  res.add_int(1);
  res.trim();
}

void big_integer::fill(size_t size, uint32_t i) {
  if (size <= length()) {
    return;
  }
  data.resize(size, i);
}

big_integer& big_integer::bitwise_impl(big_integer const& rhs,
                          std::function<uint32_t(uint32_t, uint32_t)> func) {
  if (!sign && !rhs.sign) {
    fill(rhs.length(), 0);
    for (size_t i = 0; i < length(); i++) {
      data[i] = func(data[i], rhs.data[i]);
    }
    trim();
    return *this;
  }
  big_integer a, b;
  twos_complement(a);
  rhs.twos_complement(b);
  a.sign = false;
  b.sign = false;
  a.fill(rhs.length(), sign ? MAX_DIGIT : 0);
  b.fill(length(), rhs.sign ? MAX_DIGIT : 0);
  a.bitwise_impl(b, func);
  a.sign = func(sign, rhs.sign);
  a.twos_complement(a);
  swap(a);
  return *this;
}

big_integer& big_integer::operator&=(big_integer const& rhs) {
  return bitwise_impl(rhs, [](uint32_t a, uint32_t b)
                      { return a & b; });
}

big_integer& big_integer::operator|=(big_integer const& rhs) {
  return bitwise_impl(rhs, [](uint32_t a, uint32_t b)
                      { return a | b; });
}

big_integer& big_integer::operator^=(big_integer const& rhs) {
  return bitwise_impl(rhs, [](uint32_t a, uint32_t b)
                      { return a ^ b; });
}

big_integer& big_integer::operator<<=(int rhs) {
  if (is_zero()) {
    return *this;
  }
  if (rhs >= BASE_SIZE) {
    data.insert(data.begin(), rhs / BASE_SIZE, 0);
    rhs %= BASE_SIZE;
    if (rhs) {
      *this <<= rhs;
    }
    trim();
    return *this;
  }
  if (!sign) {
    uint32_t left = rhs, right = BASE_SIZE - rhs;
    data.push_back(data.back() >> right);
    for (size_t i = length() - 2; i >= 1; i--) {
      data[i] = (data[i] << left) | (data[i - 1] >> right);
    }
    data[0] <<= rhs;
    trim();
    return *this;
  }
  big_integer res;
  twos_complement(res);
  res.data.push_back(MAX_DIGIT);
  res.sign = false;
  res <<= rhs;
  res.sign = true;
  res.data.back() |= (MAX_DIGIT >> rhs) << rhs;
  res.twos_complement(*this);
  trim();
  return *this;
}

big_integer& big_integer::operator>>=(int rhs) {
  if (rhs >= length() * BASE_SIZE) {
    if (sign) {
      *this = big_integer(-1);
    }
    else {
      *this = big_integer(0);
    }
    return *this;
  }
  if (rhs >= BASE_SIZE) {
    data.erase(data.begin(), data.begin() + rhs / BASE_SIZE);
    rhs %= BASE_SIZE;
    if (rhs) {
      *this >>= rhs;
    }
    return *this;
  }
  if (!sign) {
    uint32_t left = rhs, right = BASE_SIZE - rhs;
    for (size_t i = 0; i < length() - 1; i++) {
      data[i] = (data[i] >> left) | (data[i + 1] << right);
    }
    data.back() >>= left;
    trim();
    return *this;
  }
  big_integer res;
  twos_complement(res);
  res.data.push_back(MAX_DIGIT);
  res.sign = false;
  res >>= rhs;
  res.data.back() |= (MAX_DIGIT >> (BASE_SIZE - rhs)) << (BASE_SIZE - rhs);
  res.sign = true;
  res.twos_complement(res);
  res.trim();
  swap(res);
  return *this;
}

big_integer big_integer::operator+() const {
  return *this;
}

big_integer big_integer::operator-() const {
  big_integer res = *this;
  if (!res.is_zero()) {
    res.sign = !sign;
  }
  return res;
}

big_integer big_integer::operator~() const {
  if (!sign) {
    big_integer res;
    for (size_t i = 0; i < length(); i++) {
      res.data.push_back(~data[i]);
    }
    res.sign = true;
    res.twos_complement(res);
    return res;
  }
  big_integer a;
  twos_complement(a);
  a.sign = false;
  a = ~a;
  a.sign = false;
  a.twos_complement(a);
  a.trim();
  return a;
}

big_integer& big_integer::operator++() {
  return *this += 1;
}

big_integer big_integer::operator++(int) {
  *this += 1;
  return *this - 1;
}

big_integer& big_integer::operator--() {
  return *this -= 1;
}

big_integer big_integer::operator--(int) {
  *this -= 1;
  return *this + 1;
}

big_integer operator+(big_integer a, big_integer const& b) {
  return a += b;
}

big_integer operator-(big_integer a, big_integer const& b) {
  return a -= b;
}

big_integer operator*(big_integer a, big_integer const& b) {
  return a *= b;
}

big_integer operator/(big_integer a, big_integer const& b) {
  return a /= b;
}
big_integer operator%(big_integer a, big_integer const& b) {
  return a %= b;
}

big_integer operator&(big_integer a, big_integer const& b) {
  return a &= b;
}

big_integer operator|(big_integer a, big_integer const& b) {
  return a |= b;
}

big_integer operator^(big_integer a, big_integer const& b) {
  return a ^= b;
}

big_integer operator<<(big_integer a, int b) {
  return a <<= b;
}

big_integer operator>>(big_integer a, int b) {
  return a >>= b;
}

bool operator==(big_integer const& a, big_integer const& b) {
  return a.sign == b.sign && a.data == b.data;
}

bool operator!=(big_integer const& a, big_integer const& b) {
  return !(a == b);
}

bool operator<(big_integer const& a, big_integer const& b) {
  return b > a;
}

bool big_integer::abs_greater(big_integer const& rhs) const {
  if (length() != rhs.length()) {
    return length() > rhs.length();
  }
  for (size_t i = length() + 1; i > 1; i--) {
    size_t index = i - 2;
    if (data[index] != rhs.data[index]) {
      return data[index] > rhs.data[index];
    }
  }
  return false;
}

bool operator>(big_integer const& a, big_integer const& b) {
  if (a.sign != b.sign) {
    return b.sign;
  }
  return a.abs_greater(b) != a.sign;
}

bool operator<=(big_integer const& a, big_integer const& b) {
  return !(a > b);
}

bool operator>=(big_integer const& a, big_integer const& b) {
  return !(a < b);
}

std::string big_integer::reversed_mini_string() {
  if (length() == 0) {
    return "0";
  }
  std::string temp_str = std::to_string(data[0]);
  std::reverse(temp_str.begin(), temp_str.end());
  return temp_str;
}

std::string to_string(big_integer const& a) {
  if (a.length() == 0) {
    return "0";
  }
  big_integer res = a;
  res.sign = false;
  std::string str, temp_str("000000000");
  big_integer b10(static_cast<uint32_t>(1e9));
  do {
    str.append(std::string(9 - std::min(static_cast<size_t>(9),
                                        temp_str.length()), '0'));
    std::pair<big_integer, big_integer>qr = res.divmod(b10);
    std::swap(res, qr.first);
    temp_str = qr.second.reversed_mini_string();
    str.append(temp_str);
  } while (res != 0);
  str.append(a.sign ? "-" : "");
  std::reverse(str.begin(), str.end());
  return str;
}

std::ostream& operator<<(std::ostream& s, big_integer const& a) {
  return s << to_string(a);
}

size_t big_integer::length() const {
  return data.size();
}
