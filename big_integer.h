#pragma once

#include <iosfwd>
#include <string>
#include <vector>
#include <cstdint>
#include <limits>
#include <functional>

struct big_integer {
  big_integer();
  big_integer(big_integer const& other);
  big_integer(int a);
  big_integer(unsigned int a);
  big_integer(long long a);
  big_integer(long a);
  big_integer(unsigned long a);
  big_integer(unsigned long long a);
  explicit big_integer(std::string const& str);
  ~big_integer();

  big_integer& operator=(big_integer const& other);

  big_integer& operator+=(big_integer const& rhs);
  big_integer& operator-=(big_integer const& rhs);
  big_integer& operator*=(big_integer const& rhs);
  big_integer& operator/=(big_integer const& rhs);
  big_integer& operator%=(big_integer const& rhs);

  big_integer& operator&=(big_integer const& rhs);
  big_integer& operator|=(big_integer const& rhs);
  big_integer& operator^=(big_integer const& rhs);

  big_integer& operator<<=(int rhs);
  big_integer& operator>>=(int rhs);

  big_integer operator+() const;
  big_integer operator-() const;
  big_integer operator~() const;

  big_integer& operator++();
  big_integer operator++(int);

  big_integer& operator--();
  big_integer operator--(int);

  friend bool operator==(big_integer const& a, big_integer const& b);
  friend bool operator!=(big_integer const& a, big_integer const& b);
  friend bool operator<(big_integer const& a, big_integer const& b);
  friend bool operator>(big_integer const& a, big_integer const& b);
  friend bool operator<=(big_integer const& a, big_integer const& b);
  friend bool operator>=(big_integer const& a, big_integer const& b);

  friend std::string to_string(big_integer const& a);

private:
  std::vector<uint32_t> data {};
  bool sign {false};

  void div_int(uint32_t a);
  std::pair<big_integer, big_integer> divmod(big_integer const& b);
  void mul_int(uint32_t a);
  void add_int(uint32_t a);
  big_integer& bitwise_impl(big_integer const& rhs, std::function<uint32_t(uint32_t, uint32_t)> func);

  const uint64_t BASE = 1LL << 32;
  const uint32_t BASE_SIZE = 32;
  const uint32_t MAX_DIGIT = std::numeric_limits<uint32_t>::max();

  void fill(size_t size, uint32_t i);
  std::string reversed_mini_string();

  void constructor(uint64_t b);
  void signed_constructor(int64_t b);


  size_t length() const;
  void twos_complement(big_integer & res) const;
  void trim();
  bool is_zero();
  bool abs_greater(big_integer const& rhs) const;
  big_integer& abs_add(big_integer const& rhs);
  big_integer& abs_subtract(big_integer const& rhs);
  void swap(big_integer& b);
};

big_integer operator+(big_integer a, big_integer const& b);
big_integer operator-(big_integer a, big_integer const& b);
big_integer operator*(big_integer a, big_integer const& b);
big_integer operator/(big_integer a, big_integer const& b);
big_integer operator%(big_integer a, big_integer const& b);

big_integer operator&(big_integer a, big_integer const& b);
big_integer operator|(big_integer a, big_integer const& b);
big_integer operator^(big_integer a, big_integer const& b);

big_integer operator<<(big_integer a, int b);
big_integer operator>>(big_integer a, int b);

bool operator==(big_integer const& a, big_integer const& b);
bool operator!=(big_integer const& a, big_integer const& b);
bool operator<(big_integer const& a, big_integer const& b);
bool operator>(big_integer const& a, big_integer const& b);
bool operator<=(big_integer const& a, big_integer const& b);
bool operator>=(big_integer const& a, big_integer const& b);

std::string to_string(big_integer const& a);
std::ostream& operator<<(std::ostream& s, big_integer const& a);
